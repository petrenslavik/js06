﻿class Hamburger {
    constructor(size, stuffing) {
        if (!((size == Hamburger.SIZE_SMALL || size == Hamburger.SIZE_BIG) && (stuffing == Hamburger.STUFFING_CHEESE || stuffing == Hamburger.STUFFING_POTATO || stuffing == Hamburger.STUFFING_SALAD)))
            throw new HamburgerException("Wrong size or stuffing");
        this.size = size;
        this.stuffing = stuffing;
        this.topping = {};
    }

    addTopping(topping) {
        if (!(topping == Hamburger.TOPPING_MAYO || topping == Hamburger.TOPPING_SPICE))
            throw new HamburgerException("Wrong topping was provided");
        if (this.topping[topping])
            throw new HamburgerException("duplicate topping " + topping);
        this.topping[topping] = 1;
    }

    removeTopping(topping) {
        if (!(topping == Hamburger.TOPPING_MAYO || topping == Hamburger.TOPPING_SPICE))
            throw new HamburgerException("Wrong topping was provided");
        if (!this.topping[topping])
            throw new HamburgerException("Provided topping not found");
        this.topping[topping] = 0;
    }

    getTopping() {
        var arr = [];
        if (this.topping[Hamburger.TOPPING_MAYO])
            arr.push(Hamburger.TOPPING_MAYO);
        if (this.topping[Hamburger.TOPPING_SPICE])
            arr.push(Hamburger.TOPPING_SPICE);
        return arr;
    }

    getSize() {
        return this.size;
    }

    getStuffing() {
        return this.stuffing;
    }

    calculatePrice() {
        var price = 0;
        if (this.size === Hamburger.SIZE_SMALL)
            price += 50;
        else
            price += 100;
        if (this.stuffing === Hamburger.STUFFING_CHEESE)
            price += 10;
        else
            if (this.stuffing === Hamburger.STUFFING_POTATO)
                price += 15;
            else
                price += 20;
        if (this.topping[Hamburger.TOPPING_MAYO] === 1)
            price += 20;
        if (this.topping[Hamburger.TOPPING_SPICE] === 1)
            price += 15;
        return price;
    }

    calculateCalories() {
        var calories = 0;
        if (this.size === Hamburger.SIZE_SMALL)
            calories += 20;
        else
            calories += 40;
        if (this.stuffing === Hamburger.STUFFING_CHEESE)
            calories += 20;
        else
            if (this.stuffing === Hamburger.STUFFING_POTATO)
                calories += 10;
            else
                calories += 5;
        if (this.topping[Hamburger.TOPPING_MAYO] === 1)
            calories += 5;
        return calories;
    }
}

Hamburger.SIZE_SMALL = "small";
Hamburger.SIZE_BIG = "big";
Hamburger.STUFFING_CHEESE = "cheese";
Hamburger.STUFFING_SALAD = "salad";
Hamburger.STUFFING_POTATO = "potato";
Hamburger.TOPPING_MAYO = "mayo";
Hamburger.TOPPING_SPICE = "spice";

class HamburgerException extends Error{
    constructor(message) {
        super();
        this.message = message;
        this.name = "HamburgerException";
    }
}